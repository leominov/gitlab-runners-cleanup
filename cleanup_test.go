package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/xanzy/go-gitlab"
)

type fakeGitlabClient struct {
	runners        []*gitlab.Runner
	deletedRunners map[int]struct{}
}

func (f *fakeGitlabClient) GetBaseURL() string {
	return "http://fake-gitlab.local"
}

func (f *fakeGitlabClient) ListRunners() ([]*gitlab.Runner, error) {
	return f.runners, nil
}

func (f *fakeGitlabClient) RemoveRunner(runnerID int) error {
	_, ok := f.deletedRunners[runnerID]
	if ok {
		return fmt.Errorf("runner %d already deleted", runnerID)
	}
	f.deletedRunners[runnerID] = struct{}{}
	return nil
}

func TestCleanup(t *testing.T) {
	cli := &fakeGitlabClient{
		runners: []*gitlab.Runner{
			{
				ID:          1,
				Description: "runner1",
				IPAddress:   "127.0.0.1",
			},
			{
				ID:          2,
				Description: "runner1",
				IPAddress:   "127.0.0.1",
			},
			{
				ID:          3,
				Description: "runner1",
				IPAddress:   "127.0.0.1",
			},
			{
				ID:          4,
				Description: "runner2",
				IPAddress:   "127.0.0.1",
			},
		},
		deletedRunners: make(map[int]struct{}),
	}

	err := Cleanup(cli, true)
	if assert.NoError(t, err) {
		assert.Equal(t, 0, len(cli.deletedRunners))
	}

	err = Cleanup(cli, false)
	if assert.NoError(t, err) {
		assert.Equal(t, 2, len(cli.deletedRunners))
	}

	err = Cleanup(cli, false)
	assert.NoError(t, err)
}
