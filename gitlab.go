package main

import (
	"strings"

	"github.com/xanzy/go-gitlab"
)

type GitlabInterface interface {
	GetBaseURL() string
	ListRunners() ([]*gitlab.Runner, error)
	RemoveRunner(runnerID int) error
}

type GitlabClient struct {
	baseURL string
	cli     *gitlab.Client
}

func NewGitlabClient(token, baseURL string) (*GitlabClient, error) {
	baseURL = strings.TrimSuffix(baseURL, "/")
	cli, err := gitlab.NewClient(token, gitlab.WithBaseURL(baseURL))
	if err != nil {
		return nil, err
	}
	return &GitlabClient{
		baseURL: baseURL,
		cli:     cli,
	}, nil
}

func (g *GitlabClient) GetBaseURL() string {
	return g.baseURL
}

func (g *GitlabClient) ListRunners() ([]*gitlab.Runner, error) {
	return listRunners(g.cli)
}

func (g *GitlabClient) RemoveRunner(runnerID int) error {
	_, err := g.cli.Runners.RemoveRunner(runnerID)
	return err
}

func listRunners(gitlabCli *gitlab.Client) ([]*gitlab.Runner, error) {
	var runners []*gitlab.Runner
	page := 1
	for {
		options := &gitlab.ListRunnersOptions{
			ListOptions: gitlab.ListOptions{
				Page: page,
			},
		}
		fetchedRunners, r, err := gitlabCli.Runners.ListAllRunners(options, nil)
		if err != nil {
			return nil, err
		}
		runners = append(runners, fetchedRunners...)
		if r.NextPage == 0 {
			break
		}
		page = r.NextPage
	}
	return runners, nil
}
