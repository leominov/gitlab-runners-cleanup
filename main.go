package main

import (
	"flag"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

var (
	baseURL  = flag.String("u", "https://gitlab.qleanlabs.ru", "Gitlab address")
	token    = flag.String("t", "", "Gitlab API token")
	readOnly = flag.Bool("read-only", true, "Runs without removing runners")
)

type runner struct {
	*gitlab.Runner
	ToDelete bool
}

func main() {
	flag.Parse()

	cli, err := NewGitlabClient(*token, *baseURL)
	if err != nil {
		logrus.Fatal(err)
	}

	err = Cleanup(cli, *readOnly)
	if err != nil {
		logrus.Fatal(err)
	}
}
