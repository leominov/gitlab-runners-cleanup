# gitlab-runners-cleanup

Удаление старых неактуальных Gitlab Runner'ов, которые возникают в следствии рестарта контейнера в кластере Kubernetes. Утилита может не удалить абсолютно всех неактуальных раннеров, так как API-метод не возвращает время последней активности, которую можно было бы использовать, поэтому на каждую уникальную пару IP+Имя остается одна запись.

## Применение

По-умолчанию работает в режиме чтения, чтобы ознакомиться со списком на удаление.

```shell
Usage of ./gitlab-runners-cleanup:
  -read-only
    	Runs without removing runners (default true)
  -t string
    	Gitlab API token
  -u string
    	Gitlab address (default "https://gitlab.qleanlabs.ru")
```
