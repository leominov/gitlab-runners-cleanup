package main

import (
	"fmt"
	"sort"

	"github.com/sirupsen/logrus"
)

func Cleanup(cli GitlabInterface, readOnly bool) error {
	var (
		runnerSlice []string
		runnerMap   = make(map[string][]*runner)
	)

	runners, err := cli.ListRunners()
	if err != nil {
		return fmt.Errorf("failed to list runners: %v", err)
	}

	logrus.Infof("Runners found: %d", len(runners))

	for _, gitlabRunner := range runners {
		runnerMapID := fmt.Sprintf("%s (%s)",
			gitlabRunner.Description,
			gitlabRunner.IPAddress,
		)

		r := &runner{
			ToDelete: true,
			Runner:   gitlabRunner,
		}

		runnerMap[runnerMapID] = append(runnerMap[runnerMapID], r)
	}

	for ip := range runnerMap {
		runnerSlice = append(runnerSlice, ip)
		runnerMap[ip][len(runnerMap[ip])-1].ToDelete = false
	}

	sort.Strings(runnerSlice)

	for _, ip := range runnerSlice {
		logrus.Info(ip)
		for _, runner := range runnerMap[ip] {
			logrus.Infof("#%d %s (%s, url=%s/admin/runners/%d, delete=%v)\n",
				runner.ID,
				runner.Description,
				runner.IPAddress,
				cli.GetBaseURL(),
				runner.ID,
				runner.ToDelete,
			)

			if !runner.ToDelete || readOnly {
				continue
			}

			if err := cli.RemoveRunner(runner.ID); err != nil {
				logrus.WithError(err).Error("Failed to remove runner")
				continue
			}

			logrus.Info("Runner removed")
		}
	}

	return nil
}
