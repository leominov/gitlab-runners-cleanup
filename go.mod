module gitlab.qleanlabs.ru/platform/infra/gitlab-runners-cleanup

go 1.16

require (
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.4.0
	github.com/xanzy/go-gitlab v0.50.4
)
